const express = require("express");

const app = express();
const port = 8000;
const {router} = require("./app/routes/router")
app.use("/",router)

app.listen(port,()=>{
    console.log("app listening")
})