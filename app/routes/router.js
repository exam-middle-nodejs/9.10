const express = require("express");
const path = require("path")
const {printDateMiddleware,printUrlMiddleware} = require("../middlewares/middleware")
const router = express.Router();
router.use(express.static(__dirname + "/view"))
//router.use(printDateMiddleware,printUrlMiddleware)
router.get("/*",printDateMiddleware,printUrlMiddleware,(req,res)=>{
    
    res.sendFile(path.join(__dirname+"/view/login.html"))
})

module.exports={router}