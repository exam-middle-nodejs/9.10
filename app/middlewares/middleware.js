
const printDateMiddleware = (req,res,next)=>{
    console.log("Time", new Date());
    next()
}

const printUrlMiddleware = (req,res,next)=>{
    console.log("url: "+req.path)
    console.log("method: "+req.method)
    next()
}
module.exports={printDateMiddleware,printUrlMiddleware}